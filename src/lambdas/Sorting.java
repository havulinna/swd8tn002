package lambdas;

import java.util.Arrays;
import java.util.List;

public class Sorting {

    public static void main(String[] args) {
        List<String> languages = Arrays.asList("javaScript", "Python", "Java", "Ruby", "php");

        /*
         * TODO: use the sort() method of the list and a lambda expression to compare
         * the names of the programming languages in a case-insensitive manner. The case
         * insensitive comparison can be done inside the lambda expression with the
         * String's compareToIgnoreCase() method.
         */

        System.out.println(languages); // Should print out [Java, javaScript, php, Python, Ruby]
    }
}
