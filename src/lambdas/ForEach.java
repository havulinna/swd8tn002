package lambdas;

import java.util.Arrays;
import java.util.List;

public class ForEach {

    public static void main(String[] args) {
        List<String> languages = Arrays.asList("JavaScript", "Python", "Java", "Ruby", "PHP");

        /*
         * TODO: use the list's forEach-method and a lambda expression to print each
         * language on its own line.
         */

    }
}
