package streams;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class TotalNumberOfNames {

    public static void main(String[] args) throws IOException {

        Stream<String> stream = Files.lines(Paths.get("etunimet.csv"), Charset.forName("UTF-8"));

        /*
         * TODO:
         * 
         * 1. use the stream of names, counts and genders created above
         * (name;count;gender)
         * 
         * 2. use the map() method and split each line by semicolons to form an array
         * 
         * 3. take the second value from the array (the count)
         * 
         * 4. remove the space from the number: from "123 456" to "123456"
         * 
         * 5. parse the numeric string into an integer
         * 
         * 6. use reduce() method to accumulate all numbers from the stream together
         * 
         * 7. call the get() method on the result to get the actual value
         */

    }
}
