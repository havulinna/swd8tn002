package streams;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DistinctNames {

    public static void main(String[] args) throws IOException {

        /*
         * TODO: Modify the code so that you map the stream of rows into a map of names.
         * This should be done by splitting each row by the semicolons (;) and by taking
         * only the first part of the resulting array. Then use the distinct() method to
         * remove duplicate names from the stream and finally call count() to get the
         * number of distinct names in the stream.
         */
        long count = Files.lines(Paths.get("etunimet.csv"), Charset.forName("UTF-8")).count();

        System.out.println("Distinct names: " + count); // The correct output should be 5101
    }
}
