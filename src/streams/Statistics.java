package streams;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Statistics {

    public static void main(String[] args) throws IOException {

        /*
         * You can get the lines from the CSV file as a stream with the Files.lines()
         * method. Make sure to create a new stream for both female and male, as the
         * streams can only be consumed once:
         */
        Stream<String> stream = Files.lines(Paths.get("etunimet.csv"), Charset.forName("UTF-8"));

        /*
         * TODO: Create two streams and use their filter() and count() methods to count
         * how many names are listed for women and men.
         */
        long female = 0;
        long male = 0;

        System.out.println("Female: " + female);
        System.out.println("Male: " + male);
    }
}
